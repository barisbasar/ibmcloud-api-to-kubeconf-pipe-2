#!/usr/bin/env bash
#
# This pipe generates an (Base64 encoded) kubeconf file for your IBM Cloud Kubernetes service using an IAM# API key. 
#
# Requiered globals: 
#	API_KEY
#	CLUSTER_ID
#	OUTPUT_PATH
#
# Artifacts:
#	OUTPUT_PATH/kubeconf
#


source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
API_KEY=${API_KEY:?'NAME environment variable missing.'}
CLUSTER_ID=${CLUSTER_ID:?'NAME environment variable missing.'}
OUTPUT_PATH=${OUTPUT_PATH:?'NAME environment variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}

# Generate token from API_KEY
DIRTY_TOKEN=$(curl --location --request POST 'https://iam.cloud.ibm.com/identity/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Basic Yng6Yng=' \
--data-urlencode "apikey=$API_KEY" \
--data-urlencode 'response_type=cloud_iam' \
--data-urlencode 'grant_type=urn:ibm:params:oauth:grant-type:apikey')

# Get Clean tokens
BEARER_TOKEN=$(echo $DIRTY_TOKEN | awk -F\" '{print $4}' )
REFRESH_TOKEN=$(echo $DIRTY_TOKEN | awk -F\" '{print $8}' )

# Retrieve kubeconifig.zip
run curl --location --request GET "https://containers.cloud.ibm.com/global/v1/clusters/$CLUSTER_ID/config" \
--header "Authorization: $BEARER_TOKEN" \
--header "X-Auth-Refresh-Token: $REFRESH_TOKEN" >> kubeconfig.zip

run unzip kubeconfig.zip
run cp kubeConfig000000000/* .

# encode bse64 pem
PEM=$(base64 *.pem | tr -d '\n\r') 

# replace PEM base64
run sed -i '/certificate-authority/c\ certificate-authority-data: '"$PEM"'' kube*.yml
run grep cluster kube*.yml

# encode YAML
run base64 kube*.yml --output_file=kubeconf

# export kubeconf
run mkdir $OUTPUT_PATH
run cp kubeconf $OUTPUT_PATH/kubeconf
WC=$(wc $OUTPUT_PATH/kubeconf) 
info "wc kubeconf: $wc" 
if [[ "${status}" == "0" ]]; then
  success "Success! Output file: $OUTPUT_PATH/kubeconf"
else
  fail "Error!"
fi
